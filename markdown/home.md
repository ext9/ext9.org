### [N-Chain](https://n-chain.org) Project Ext9
A community driven blockchain power by chia's PosT.
#### Highlights
|            |                        |
| ---------- | ---------------------- |
| Reward     | same as chia-mainnet   |
| Prefarming | 2 for testnet9's owner |
| NFT-Plot   | Support                |
| co-farming   | Support                |

[download](https://ext9.org/download/binary) |
[blockfile](https://ext9.org/download/db/) |
[gitee](https://gitee.com/ext9) |
[discord](https://discord.com/channels/872362222049976350)|
[faq](#/3)


